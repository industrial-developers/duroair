Duroair Static Site
========================

This static (JAM stack) site is generated via [metalsmith](http://www.metalsmith.io/) from markdown content files. The `build` directory is what is expected to be deployed for serving to the public.

### CMS
There is a [Decap CMS](https://decapcms.org/ instance in place for editing the markdown files (accessible at /admin/).
Access to the CMS is controlled by netlify identity and the git-gateway service.

The CMS admin config file is generated/composed by combining partial files into a larger file. This is done for DRY principles and uses
the npm preprocess module for file manipulation.


Getting Started
----------------
- 
- Clone the repo
- `npm install`
- `npm start` to build the site and begin serving


Folder Structure
-----------------------
````
site-root
|-build
|-layouts
|   |-partials
|-lib
|-public
|-functions
|-src
|  |-admin
|  |-content
|  |-js
|  |-less
|-tools
````

`build` - This folder is .gitignored and generate from running `npm run build`. Don't edit anything in this folder your edits will be destroyed. This is where things are output for hosting on netlify.

`layouts` - This folder contains the main site layout and other page layouts. Layouts us [Nunjucks](https://mozilla.github.io/nunjucks/templating.html) for templating. Partials and other 'included' files related to layout are also here.

`lib` - This folder contains custom plugins for use in metalsmith.

`public` - This folder is merged into the `build` folder during the build process. It contains things like favicons, pdfs, imgs, etc. - stuff the site needs that is not created during the build.

`src` - This is where the magic happens. Content is located here in markdown files along with less & js files. The admin portion is also here.

`functions` - any 'dynamic' things requiring server side code - search for example

Building
--------

`npm run build` is the build command. This command compiles js & less,  copies everything from the 'public' folder, creates the admin config file, 


Deploying
---------

Deploy to netlify.com via pushing to the git remote origin and publishing via netlify.

Redirects
---------

Redirects can be specified in the `public/_redirects` folder. Check the (Netlify docs)[https://www.netlify.com/docs/redirects/] on this topic.


Netlify Functions
--------------------
Search is handled by Netlify functions
https://docs.netlify.com/functions/overview/



### Useful Info
- image-1 size 683 484 
- image-2 size 1000 
- image-3 size 636 484
- product image 359 x 293
- product thumb 359 x 254
- resizing with sharp for f in *.jpg; do sharp -i $f -o "../thumbs/$f" resize 324 293 ;done
- blc -reof --host-requests=10   --input https://romantic-noether-3b5a7f.netlify.com



TODO:
- pagination blocks next previous
