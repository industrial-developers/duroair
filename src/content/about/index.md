---
layout: about.njk
title: About Duroair
seo:
  description: Duroair knows why lean manufacturing has unique needs for facility
    design and air purification, and creates industrial air systems to meet your
    exact needs.
  page_title: About Us | Why Lean Manufacturing?
nav_title: About
nav_sort: 4500
show_in_menu: "true"
jumbotron:
  img: /img/heros/hero-about.jpg
image_1: /img/image-1-about2.jpg
copy_block_1: >-
  ### Innovating Industrial Clean Air Solutions


  Never satisfied with the status quo, we’re always searching for new technology and methods for creating a clean air competitive advantage:


  * **Hexavalent Chromium Protection:** Through independent testing, Duroair has verified that our exhaust systems meet or exceed NESHAP 319 standards by capturing and containing hexavalent chromium – allowing clean air to be recirculated back into the manufacturing space or vented outside.

  * **Dust Collection Innovation:** Because your employees are your most valuable assets, Duroair has developed [DuroDust™](/products/durodust), a dust collection system that provides a properly-vented, regulated work area that limits employees’ exposure to respirable silica, metal dust, and other particulates.

  * **Space-Saving Hinge Design:** Space efficiency is always a priority, so Duroair has created a new hinge design that does not impact the enclosure footprint and moves up in the vertical wall axis, while the building retracts to under 20 percent of its extended length.
image_2: /img/image-2-aerospace.jpg
cards:
  - copy: Duroair engineers custom solutions that meet EHS requirements and other
      clean air compliance standards while improving the bottom line.
    image: /img/beyond-compliance-card-img.jpg
    title: Beyond Compliance
    url: /about/beyond-compliance/
  - copy: Duroair’s consultative approach to engineering clean air solutions is
      always responsive to manufacturers’ environmental, safety, and budgetary
      needs.
    image: /img/how-we-work-card-img.jpg
    title: How We Work
    url: /how-we-work/
---
### Making the Business Case<br>for Clean Air

At Duroair, we believe everyone deserves a clean, productive and safe working environment. For more than a decade, we’ve invested in research and development to design and manufacture industrial clean air solutions that not only meet EHS compliance, but also have a measurable impact on the manufacturing bottom line.  

From North America to Asia, Europe and beyond, we’re committed to:

* Improving overall health and safety in lean manufacturing facilities
* Engineering [solutions](/solutions) to isolate, capture, and contain a wide variety of industrial processes
* Going [beyond EHS compliance](/about/beyond-compliance/) to maximize efficiency and productivity