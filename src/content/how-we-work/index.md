---
layout: how-we-work.njk
title: How We Work
show_in_menu: 'true'
nav_sort: 4000
jumbotron:
  img: /img/heros/hero-howwework.jpg
seo:
  page_title: How We Work | Lean Production System
  description: Duroair’s filtration systems are adaptable, ready to change with the evolving processes of a lean production system.
testimonial:
  attestant: Pete M.
  company: Manufacturing Manager, AZZ Inc.
  quotation: >-
    The purchase of our Duroair system has allowed us to bring the coatings
    process in-house without permanently giving up our shop floor space. As a
    result, we have increased our customer responsiveness, as well as reduced
    our operating cost while providing a safe environment for our employees.
blocks:
    - icon: /img/icons/icon-basic_magnifier.svg
      copy: Analyze your technical, space, and budgetary requirements
    - icon: /img/icons/icon-basic_pencil_ruler.svg
      copy: Propose a tailored solution to improve your manufacturing compliance, efficiency, and productivity
    - icon: /img/icons/icon-basic_settings.svg
      copy: Design, produce, and install your customized retractable enclosure and air handling unit
    - icon: /img/icons/icon-basic_sheet_pen.svg
      copy: Follow up to ensure your solution is fully compliant and creating a cleaner, safer, more productive working environment
cta:
  headline: Make Clean Air a Competitive Advantage
  copy: Contact us today to discuss a compliant clean air solution that delivers bottom-line results in your manufacturing facility.
  label: Request a Quote
---
