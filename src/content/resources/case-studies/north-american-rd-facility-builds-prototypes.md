---
title: Climate-Controlled Clean Room Enables Maneuverable Aerospace Prototyping
seo:
  description: >-
    Case Study: North American R&D facility of leading global security company
    builds prototypes of unmanned airborne systems for the purpose of testing.
  page_title: Climate-Controlled Clean Room Case Study
jumbotron:
  headline: Maximized Material Flow
  img: /img/case-study/Cleanroom_2.png
layout: case-study.njk
headline: North American Aerospace Prototype Testing and Validation
short_description: Climate-controlled clean room enables maneuverable aerospace prototyping.
situation: "The North American R&D facility of a U.S.-based leading global security company\_builds prototypes of unmanned airborne systems for the purpose of\_testing and validation. Parts are produced in limited numbers throughout the fabricating shop. Component building process involves\_the use of adhesives that will only successfully bond within very specific\_temperature and humidity ranges that require climate control. In addition,\_the bonding process requires a clean room environment and has to be\_applied \"in-place\" at those shop locations where various components were\_being built. Conventional air make-up requirements don't allow for such a\_cellular fabricating model, while variable climate control in a fabricating\_environment is a delicate issue. Due to the intermittent need for the\_adhesive application function, a conventional clean room would reduce the\_usable existing floor space of the production facility."
solution: "Duroair suggested the use of a unique, climate-controlled [clean room](https://www.duroair.com/technologies-solutions/clean-room-solutions/ \"Cleanroom Solutions\")\_design that includes its portable, non-vented [DuroPure™](https://www.duroair.com/technologies-solutions/non-vented-air-recycling-filtration-solutions/ \"Non-Vented Air Recycling Filtration Solutions\")\_indoor air purification system equipped with a Tier-sourced, climate control unit in combination with a custom-designed [DuroRoom™](https://www.duroair.com/technologies-solutions/retractable-enclosure-systems/ \"Retractable Enclosure Systems\") retractable enclosure. The hybrid [DuroPure™](https://www.duroair.com/technologies-solutions/non-vented-air-recycling-filtration-solutions/ \"Non-Vented Air Recycling Filtration Solutions\")\_unit delivers both a Class 100,000 (ISO8) clean room environment plus required temperature and humidity control. The [DuroRoom™](https://www.duroair.com/technologies-solutions/retractable-enclosure-systems/ \"Retractable Enclosure Systems\") enclosure retracts to 20% of the extended length when not in use."
results: "Efficient material flow is accomplished by maneuvering the retracted Duroair [clean room](/solutions/clean-rooms)\_to the component after which the enclosure is extended around the\_component prior to the adhesive application. Temperature and humidity are\_efficiently and cost-effectively controlled while particulate and carbon filters\_provide required air quality. The use of existing floor space is maximized and no equipment and installation costs for air\_makeup are added."
results_image: /img/case-study/maximized-material-flow-results.jpg
---

